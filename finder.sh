#!/bin/bash

NEWLOC=`curl -L "https://www.soundsonline.com/support/updates" 2>/dev/null | /usr/local/bin/htmlq -a href a | grep Play | grep .zip | head -1 `

if [ "x${NEWLOC}" != "x" ]; then
	echo ${NEWLOC}
fi
