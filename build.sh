#!/bin/bash -ex

# CONFIG
prefix="EastWestPlay"
suffix=""
munki_package_name="EastWestPlay"
display_name="EastWest Play"
icon_name=""
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.zip -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
#mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
unzip app.zip
rm -rf *Uninstaller.pkg
app_pkg=$(ls *.pkg)
mv "$app_pkg" app.pkg
/usr/sbin/pkgutil --expand app.pkg pkg
mkdir -p "build-root/Library/Application Support/East West"
#cp -R $app_in_dmg build-root/Applications

(cd build-root; cd Library; cd Application\ Support; cd East\ West; pax -rz -f ../../../../pkg/Play_Standalone.pkg/Payload)

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" "build-root/Library/Application Support/East West/Play.app/Contents/Info.plist"`
#identifier=`/usr/libexec/PlistBuddy -c "Print :CFBundleIdentifier" $app_in_dmg/Contents/Info.plist`

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 6 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo app.pkg "${key_files}" | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist
## END EXAMPLE

# Build pkginfo
#/usr/local/munki/makepkginfo app.pkg > app.plist

plist=`pwd`/app.plist

# Obtain version info
#minver=`/usr/libexec/PlistBuddy -c "Print :installs:0:minosversion" "${plist}"`

# For silent packages, configure blocking_applications and unattended_install
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.13.0"
#defaults write "${plist}" minimum_os_version "${minver}"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" requires -array iLokLicenseManager
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
